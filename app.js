const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const cartRoutes = require('./routes/cartRoutes')
const orderRoutes = require('./routes/orderRoutes')

dotenv.config()

const app = express()
const port = 8000
//const port = process.env.PORT || 8000

mongoose.connect(`mongodb+srv://gnbr0511:${process.env.MONGODB_PASSWORD}@cluster0.h8bn8fo.mongodb.net/e-commerce?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
let db = mongoose.connection

db.once('open', () => console.log('Connected to MongoDB!'))

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//ROUTES
app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)
app.use('/carts', cartRoutes)
//ROUTES END

app.listen(port, () => console.log(`E-commerce API is now running at 127.0.0.1:${port}...`))
//app.listen(process.env.PORT || port, () => console.log(`E-commerce API is now running at 127.0.0.1:${process.env.PORT || port}...`))