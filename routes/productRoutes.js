const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

//CREATE PRODUCT
router.post('/create-product', auth.verifyUser, (request, response) => {
	let data = {
		productDetails: request.body,
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin
	}
	ProductController.createProduct(data).then((productCreationResult) => response.send(productCreationResult))
})

//RETRIEVE ACTIVE && APPROVED PRODUCTS BY ADMIN
router.get('/', (request, response) => {
	ProductController.getActiveProducts().then((retrievedProducts) => response.send(retrievedProducts))
})

//RETRIEVE USER/SELLER'S ADDED PRODUCTS
router.get('/my-products', auth.verifyUser, (request, response) => {
	let data = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin
	}
	ProductController.getUsersProducts(data).then((retrievedProducts) => response.send(retrievedProducts))
})

//RETRIEVE UNAPPROVED/NEW PRODUCTS ADDED BY USERS FOR ADMIN REVIEW (admin only)
router.get('/unapproved', auth.verifyUser, (request, response) => {
	let isAdmin = auth.decodeUserToken(request.headers.authorization).isAdmin
	ProductController.getUnapprovedProducts(isAdmin).then((retrievalResult) => response.send(retrievalResult))
})

//RETRIEVE ALL PRODUCTS (active or not active or approve or not approve by admins) (admin only)
router.get('/all', auth.verifyUser, (request, response) => {
	let isAdmin = auth.decodeUserToken(request.headers.authorization).isAdmin
	ProductController.getAllProducts(isAdmin).then((retrievalResult) => response.send(retrievalResult))
})

//APPROVE A PRODUCT/S ADDED BY USERS (admin only)
router.patch('/:productId/approve', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		productId: request.params.productId
	}
	ProductController.approveProduct(data).then((approvingResult) => response.send(approvingResult))
})

//BAN A PRODUCT (admin only)
router.patch('/:productId/ban', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		productId: request.params.productId
	}
	ProductController.banProduct(data).then((banningResult) => response.send(banningResult))
})

//ARCHIVE A PRODUCT (user only) - a product can only be archived by a user/seller who created it
router.patch('/:productId/archive', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		productId: request.params.productId
	}
	ProductController.archive(data).then((archivingResult) => response.send(archivingResult))
})

//UNARCHIVE A PRODUCT
router.patch('/:productId/unarchive', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		productId: request.params.productId
	}
	ProductController.unArchive(data).then((Result) => response.send(Result))
})

//SEARCH PRODUCT BY NAME - this can also be done by unauthenticated user. Just like lazada and shopee websites
router.post('/search', (request, response) => {
	ProductController.searchProductsByName(request.body.searchKeyword).then((retrievalResult) => response.send(retrievalResult))
})

//ADMIN search
router.post('/admin-search', (request, response) => {
	ProductController.searchProductsByNameAdmin(request.body.searchKeyword).then((retrievalResult) => response.send(retrievalResult))
})

//For sellers to search from their own products
router.post('/my-products/search', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		searchKeyword: request.body.searchKeyword
	}
	ProductController.searchFromMyProductsByName(data).then((retrievalResult) => response.send(retrievalResult))
})

//UPDATE PRODUCT (user) - Only the user who created the product can update the product details
router.patch('/:productId/update', auth.verifyUser, (request, response) => {
	let userData = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin
	}
	ProductController.updateProduct(userData, request.params.productId, request.body).then((updateResult) => response.send(updateResult))
})

//RETRIEVE SINGLE PRODUCT
router.get('/:productId', (request, response) => {
	ProductController.getSingleProduct(request.params.productId).then((retrievalResult) => response.send(retrievalResult))
})


module.exports = router