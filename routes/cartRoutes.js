const express = require('express')
const router = express.Router()
const CartController = require('../controllers/CartController')
const auth = require('../auth')

//ADD TO CART A PRODUCT
router.post('/:productId/add-to-cart', auth.verifyUser, (request, response) => {
	let data = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		productId: request.params.productId,
		quantity: request.body.quantity
	}
	CartController.addToCart(data).then((addToCartResult) => response.send(addToCartResult))
})

//RETRIEVE LOGGED-IN-USER'S CART ITEMS
router.get('/my-cart', auth.verifyUser, (request, response) => {
	let userData = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin
	}
	CartController.getCartItems(userData).then((cartItemsRetrievalResult) => response.send(cartItemsRetrievalResult))
})

//CHECK OUT LOGGED-IN-USER'S CART ITEMS
router.post('/check-out', auth.verifyUser, (request, response) => {
	let data = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		paymentMethod: request.body.paymentMethod,
		amountPaid: request.body.amountPaid
	}
	CartController.checkOut(data).then((checkOutResult) => response.send(checkOutResult))
})

//CHANGE PRODUCT QUANTITY
router.patch('/product/:id/change-qty', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		productId: request.params.id,
		newQty: request.body.quantity
	}
	CartController.updateQuantity(data).then((updateResult) => response.send(updateResult))
})

//REMOVE ITEM FROM THE CART
router.patch('/product/:id/remove', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		productId: request.params.id
	}
	CartController.removeItem(data).then((updateResult) => response.send(updateResult))
})

module.exports = router
