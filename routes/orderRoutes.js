const express = require('express')
const router = express.Router()
const auth = require('../auth')
const OrderController = require('../controllers/OrderController')

//GET LOGGED-IN-USER'S ORDERS
router.get('/my-orders', auth.verifyUser, (request, response) => {
	let userData = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		userId: auth.decodeUserToken(request.headers.authorization).userId
	}
	OrderController.getMyOrders(userData).then((retrievalResult) => response.send(retrievalResult))
})

//GET ALL ORDERS FROM ALL USERS (admin only)
router.get('/', auth.verifyUser, (request, response) => {
	let isAdmin = auth.decodeUserToken(request.headers.authorization).isAdmin
	OrderController.getAllOrders(isAdmin).then((retrievalResult) => response.send(retrievalResult))
})

//GET ALL ORDERS FROM LOGGED-IN-USER'S PRODUCTS
router.get('/my-products-orders',  auth.verifyUser, (request, response) => {
	let userData = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin
	}
	OrderController.getMyProductsOrders(userData).then((retrievalResult) => response.send(retrievalResult))
})

//SEARCH ORDERS FROM LOGGED-IN-USER'S PRODUCTS
router.get('/my-products-orders/search',  auth.verifyUser, (request, response) => {
	let userData = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		searchKeyword: request.body.searchKeyword
	}
	OrderController.searchMyProductsOrders(userData).then((retrievalResult) => response.send(retrievalResult))
})

//GET ALL PENDING ORDERS FROM LOGGED-IN-USER'S PRODUCTS
router.get('/my-products-orders/pending',  auth.verifyUser, (request, response) => {
	let userData = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin
	}
	OrderController.getMyProductsOrdersPending(userData).then((retrievalResult) => response.send(retrievalResult))
})

//ACCEPT ORDER
router.patch('/:orderedProductId/accept', auth.verifyUser, (request, response) => {
	let data = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		orderProductId: request.params.orderedProductId
	}
	OrderController.acceptOrder(data).then((acceptOrderResult) => response.send(acceptOrderResult))
})

//SHIPPED ORDER STATUS
router.patch('/:orderedProductId/ship', auth.verifyUser, (request, response) => {
	let data = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		orderProductId: request.params.orderedProductId
	}
	OrderController.shipOrder(data).then((shippingOrderResult) => response.send(shippingOrderResult))
})

//DELIVERED ORDER STATUS
router.patch('/:orderedProductId/deliver', auth.verifyUser, (request, response) => {
	let data = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		orderProductId: request.params.orderedProductId
	}
	OrderController.deliverOrder(data).then((deliveringOrderResult) => response.send(deliveringOrderResult))
})

module.exports = router