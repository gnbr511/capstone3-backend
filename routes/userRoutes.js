const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

//REGISTER USER
router.post('/register', (request, response) => {
	UserController.register(request.body).then((registrationResult) => response.send(registrationResult))
})

//LOGIN
router.post('/login', (request, response) => {
	UserController.login(request.body).then((loginResult) => response.send(loginResult))
})

//RETRIEVE USER DETAILS - get logged in user details
router.get('/details', auth.verifyUser, (request, response) => {
	let userId = auth.decodeUserToken(request.headers.authorization).userId
	UserController.getUserDetails(userId).then((retrievalResult) => response.send(retrievalResult))
})

//Getting seller name
router.get('/:sellerId', (request, response) => {
	const sellerId = request.params.sellerId
	UserController.getUserName(sellerId).then((retrievalResult) => response.send(retrievalResult))
})

//Getting customer name
router.get('/:customerId', (request, response) => {
	const customerId = request.params.customerId
	UserController.getUserName(customerId).then((retrievalResult) => response.send(retrievalResult))
})

//RETRIEVE ANY USER/SELLER DETAILS (ADMIN ONLY) - get information of a specific user by userId
router.get('/:userId/user-details', auth.verifyUser, (request, response) => {
	let data = {
		userId: request.params.userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin
	}
	UserController.getSellerDetails(data).then((retrievalResult) => response.send(retrievalResult))
})

//SET USER TO ADMIN (ADMIN ONLY)
router.patch('/:userId/set-to-admin', auth.verifyUser, (request, response) => {
	let data = {
		userId: request.params.userId,
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin
	}
	UserController.setUserToAdmin(data).then((result) => response.send(result))
})

//UPDATE USER INFORMATION
router.patch('/update-information', auth.verifyUser, (request, response) => {
	let data = {
		userId: auth.decodeUserToken(request.headers.authorization).userId,
		newInfo: request.body
	}
	UserController.updateInformation(data).then((updateResult) => response.send(updateResult))
})

//RETRIEVE ALL USERS (ADMIN ONLY)
router.get('/all-users', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin : auth.decodeUserToken(request.headers.authorization).isAdmin,
		userId: auth.decodeUserToken(request.headers.authorization).userId
	}
	UserController.getAllUsers(data).then((retrievalResult) => response.send(retrievalResult))
})

//SEARCH USER BY NAME
router.get('/search', auth.verifyUser, (request, response) => {
	let data = {
		isAdmin: auth.decodeUserToken(request.headers.authorization).isAdmin,
		searchKeyword: request.body.searchKeyword
	}
	UserController.searchUser(data).then((retrievalResult) => response.send(retrievalResult))
})


module.exports = router