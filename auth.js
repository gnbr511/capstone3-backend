const jsonWebToken = require('jsonwebtoken')

const tokenSecretPhrase = "OnlineBuyAndSellAPI"

module.exports.createAccessToken = (loggingInUser) => {
	const userData = {
		userId: loggingInUser._id,
		email: loggingInUser.email,
		isAdmin: loggingInUser.isAdmin
	}
	return jsonWebToken.sign(userData, tokenSecretPhrase, {})
}

module.exports.verifyUser = (request, response, next) => {
	let userToken = request.headers.authorization
	if(typeof userToken !== "undefined"){
		userToken = userToken.slice(7, userToken.length)
		return jsonWebToken.verify(userToken, tokenSecretPhrase, (error, data) => {
			if(error){
				return response.send({auth: 'User authentication failed!'})
			}else{
				next()
			}
		})
	}else{
		return response.send({auth: 'User authentication failed!'})
	}
}

module.exports.decodeUserToken = (userToken) => {
	if(typeof userToken !== "undefined"){
		userToken = userToken.slice(7, userToken.length)
		return jsonWebToken.verify(userToken, tokenSecretPhrase, (error, data) => {
			if(error){
				return null
			}else{
				return jsonWebToken.decode(userToken, {complete: true}).payload;
			}
		})
	}else{
		return null
	}
}