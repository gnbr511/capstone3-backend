const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "Product-creator's ID is required!"]
	},
	productName: {
		type: String,
		required: [true, 'Product name is required!']
	},
	productBrand:{
		type: String,
		required: [true, 'Product name is required!']
	},
	productDescription: {
		type: String,
		required: [true, 'Product description is required!']
	},
	price: {
		type: Number,
		required: [true, 'Product price is required!']
	},
	stocksAvailable: {
		type: Number,
		required: [true, 'Number of stocks is required!']
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	isActive: {
		type: Boolean,
		default: true
	},
	isApprovedByAdmin: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model('Product', productSchema)