const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required!']
	},
	products: [
		{
			sellerId: {
				type: String,
				required: [true, 'Seller ID is required!']
			},
			productId: {
				type: String,
				required: [true, 'Product ID is required!']
			},
			productName:{
				type: String,
				required: [true, 'Product name is required!']
			},
			productBrand: {
				type: String,
				required: [true, 'Product brand is required!']
			},
			price: {
				type: Number,
				required: [true, 'Price of ordered product is required!']
			},
			quantity: {
				type: Number,
				required: [true, 'Order quantity is required!']
			},
			subtotal: {
				type: Number,
				required: [true, 'Subtotal for each item is required!']
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, 'Total totalAmount is required!']
	}
})

module.exports = mongoose.model('Cart', cartSchema)