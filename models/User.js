const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required!']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required!']
	},
	address: {
		type: String,
		required: [true, 'Address is required!']
	},
	birthDate: {
		type: Date,
		required: [true, 'Birth date is required!']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required!']
	},
	email: {
		type: String,
		required: [true, 'Email address is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isBanned: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model('User', userSchema)