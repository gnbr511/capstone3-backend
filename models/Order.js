const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required!']
	},
	products: [
		{
			sellerId: {
				type: String,
				required: [true, 'Seller ID is required!']
			},
			productId: {
				type: String,
				required: [true, 'Product ID is required!']
			},
			productName: {
				type: String,
				required: [true, 'Product name is required!']
			},
			productBrand: {
				type: String,
				required: [true, 'Product brand is required!']
			},
			price: {
				type: Number,
				required: [true, 'Price is required!']
			},
			quantity: {
				type: Number,
				required: [true, 'Order quantity is required!']
			},
			subtotal: {
				type: Number,
				required: [true, 'Subtotal is required!']
			},
			status: {
				type: String,
				default: 'pending' //accepted -> picked up by courier -> delivered
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, 'Total amount is required!']
	},
	paymentMethod:{
		type: String,
		default: 'cash-on-delivery' //via gcash/paymaya
	},
	amountPaid: {
		type: Number,
		default: 0
	},
	isPaid: {
		type: Boolean,
		default: false
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Order', orderSchema)