const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')

//REGISTER USER
module.exports.register = (registrationDetails) => {
	//making sure that the email is not yet registered
	return User.find({email: registrationDetails.email}).then((foundUser) => {
		if(foundUser.length > 0){
			return {
				isSuccess: false,
				message: 'This email is already connected to a registered user!'
			}
		}else{
			let encryptedPassword = bcrypt.hashSync(registrationDetails.password, 10)
			let newUser = new User({
				firstName: registrationDetails.firstName,
				lastName: registrationDetails.lastName,
				address: registrationDetails.address,
				birthDate: registrationDetails.birthDate,
				mobileNo: registrationDetails.mobileNo,
				email: registrationDetails.email,
				password: encryptedPassword,
				isAdmin: registrationDetails.isAdmin
			})
			return newUser.save().then((savedUser, error) => {
				if(error){
					return {
						isSuccess: false,
						message: 'Something went wrong!'
					}
				}
				return {
					isSuccess: true,
					message: 'You have successfully registered!'
				}
			})
		}
	})
}

//LOGIN
module.exports.login = (loginCredentials) => {
	return User.findOne({email: loginCredentials.email}).then((foundUser) => {
		if(foundUser == null){
			return {
				isSuccess: false,
				message: "User doesn't exist!"
			}
		}else{
			const isPasswordCorrect = bcrypt.compareSync(loginCredentials.password, foundUser.password)
			if(isPasswordCorrect){
				return {
					isSuccess: true,
					message: "You are now logged in!",
					auth: auth.createAccessToken(foundUser)
				}
			}
			return {
				isSuccess: false,
				message: 'Password is incorrect!'
			}
		}
	})
}

//RETRIEVE USER DETAILS
module.exports.getUserDetails = (userId) => {
	return User.findById(userId).then((foundUser, error) => {
		if(error){
			return
		}
		return foundUser
	})
}

//Getting seller/buyer name
module.exports.getUserName = (id) => {
	return User.findById(id).then((foundUser, error) => {
		if(error){
			return {
				isSuccess: false,
				message: 'Something went wrong!'
			}
		}
		return {
			isSuccess: true,
			firstName: foundUser.firstName, 
			lastName: foundUser.lastName
		}
	})
}

//RETRIEVE ANY USER/SELLER DETAILS (ADMIN ONLY) - get information of a specific user/seller by userId
module.exports.getSellerDetails = (data) => {
	if(!data.isAdmin){
		return Promise.resolve({
			typeOfError: 'Accessibility error',
			message: 'You must be an admin to access this.'
		})
	}
	return User.findById(data.userId).then((foundUser, error) => {
		if(error){
			return error
		}
		return foundUser
	})
}

//SET USER TO ADMIN (ADMIN ONLY)
module.exports.setUserToAdmin = async (data) => {
	if(!data.isAdmin){
		return Promise.resolve({
			typeOfError: "Accessibility error",
			message: "Only an admin can access this."
		})
	}
	let isUserAlreadyAdmin = await User.findById(data.userId).then((foundUser) => foundUser.isAdmin)
	if(isUserAlreadyAdmin){
		return Promise.resolve({
			message: "This user is already an admin."
		})
	}
	return User.findByIdAndUpdate(data.userId, {isAdmin: true}).then((updatedUser, error) => {
		if(error){
			return error
		}
		return {message: "User has been set to admin."}
	})
}

//UPDATE USER INFORMATION
module.exports.updateInformation = (data) => {
	return User.findByIdAndUpdate(
		{_id: data.userId},
		{
			firstName: data.newInfo.firstName,
			lastName: data.newInfo.lastName,
			address: data.newInfo.address,
			birthDate: data.newInfo.birthDate,
			mobileNo: data.newInfo.mobileNo
		},
		{new: true}
	).then((updatedUser, error) => {
		if(error){
			return error
		}
		return {message: "Update successful!"}
	})
}

//GET ALL USERS
module.exports.getAllUsers = (data) => {
	if(!data.isAdmin){
		return Promise.resolve({message: "Only an admin can access this."})
	}
	return User.find({_id: {$ne: data.userId} }).then((allUsers, error) => {
		if(error){
			return error
		}
		return allUsers
	})
}

//SEARCH USER BY NAME
module.exports.searchUser = (data) => {
	if(!data.isAdmin){
		return Promise.resolve({message: "Only an admin can access this."})
	}
	return User.aggregate([
		{
			$addFields: {fullName: {$concat: ["$firstName", " ", "$lastName"]}}
		},
		{
			$match: {fullName: {$regex: data.searchKeyword, $options: "i"}}
		},
		{
			$project: {fullName: 0}
		}
	])
}