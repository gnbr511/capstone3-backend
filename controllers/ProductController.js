const Product = require('../models/Product')
const User = require('../models/User')

//CREATE PRODUCT
module.exports.createProduct = (data) => {
	if(!data.isAdmin){
		let newProduct = new Product({
			userId: data.userId,
			productName: data.productDetails.productName,
			productBrand: data.productDetails.productBrand,
			productDescription: data.productDetails.productDescription,
			price: data.productDetails.price,
			stocksAvailable: data.productDetails.stocksAvailable
		})
		return newProduct.save().then((savedProduct, error) => {
			if(error){
				return {
					isSuccess: false,
					message: 'Something went wrong!'
				}
			}
			return {
				isSuccess: true,
				message: 'New product added successfully!'
			}
		})
	}
	return Promise.resolve({
		isSuccess: false,
		message: "An admin don't create or sell a product, but can only approve or disapprove products created by users."
	})
}

//GET ACTIVE PRODUCTS (products that are active and approved by the admin)
module.exports.getActiveProducts = () => {	
	return Product.find(
		{	$and:[{isActive:true},{isApprovedByAdmin: true}]
		},
		{	productName: 1,
			productBrand: 1,
			productDescription: 1,
			price: 1,
			stocksAvailable: 1}
		)
	.then((foundProducts) => foundProducts)
}

//GET USER'S PRODUCTS (retrieve all products of a user/seller)
module.exports.getUsersProducts = (data) => {
	if(!data.isAdmin){
		return Product.find({userId: data.userId}).then((foundProducts) => foundProducts)
	}
	return Promise.resolve({
		typeOfError: "Accessibility error",
		message: "An admin don't create or sell a product, but can only approve or disapprove products created by users."
	})
}

//RETRIEVE UNAPPROVED/NEW PRODUCTS ADDED BY USERS FOR ADMIN REVIEW (admin only)
module.exports.getUnapprovedProducts = (isAdmin) => {
	if(isAdmin){
		return Product.find({isApprovedByAdmin: false}).then((foundProducts) => foundProducts)
	}
	return Promise.resolve({message: 'Access Denied! You are not an admin!'})
}

//RETRIEVE ALL PRODUCTS (active or not active or approve or not approve by admins) (admin only)
module.exports.getAllProducts = (isAdmin) => {
	if(isAdmin){
		return Product.find({}).then((foundProducts) => foundProducts)
	}
	return Promise.resolve({message: 'Access Denied! You are not an admin!'})
}

//APPROVE A PRODUCT BY ID (admin only)
module.exports.approveProduct = async (data) => {
	if(data.isAdmin){
		let isApprovedByAdmin = await Product.findById(data.productId).then((foundProduct) => foundProduct.isApprovedByAdmin)
		if(!isApprovedByAdmin){
			return Product.findByIdAndUpdate(data.productId, {isApprovedByAdmin: true}, {new: true}).then((approvedProduct, error) => {
				if(error){
					return {
						isSuccess: false,
						message: 'Something went wrong!'
					}
				}else if(approvedProduct == null){
					return {
						isSuccess: false,
						message: 'Product not found!'
					}
				}
				return {
					isSuccess: true,
					message: 'The product has been approved.',
					approvedProduct: approvedProduct
				}
			})
		}
		return Promise.resolve({
			isSuccess: false,
			message: "Product already approved!"
		})		
	}
	return Promise.resolve({
		isSuccess: false,
		message: 'Access Denied! You are not an admin!'
	})
}

//BAN A PRODUCT BY ID (admin only)
module.exports.banProduct = async (data) => {
	if(data.isAdmin){
		let isApprovedByAdmin = await Product.findById(data.productId).then((foundProduct) => foundProduct.isApprovedByAdmin)
		if(isApprovedByAdmin){
			return Product.findByIdAndUpdate(data.productId, {isApprovedByAdmin: false}, {new: true}).then((bannedProduct, error) => {
				if(error){
					return {
						isSuccess: false,
						message: 'Something went wrong! Banning product unsuccessful.'
					}
				}else if(bannedProduct == null){
					return {
						isSuccess: false,
						message: 'Product not found!'}
				}
				return {
					isSuccess: true,
					message: 'The product has been banned.',
					bannedProduct: bannedProduct
				}
			})
		}
		return Promise.resolve({message: "Product already banned or unpproved!"})		
	}
	return Promise.resolve({message: 'Access Denied! You are not an admin!'})
}

//ARCHIVE A PRODUCT (user only) - a product can only be archived by a user/seller who created it
module.exports.archive = (data) => {
	if(data.isAdmin){
		return Promise.resolve({
		isSuccess: false,
		message: "An admin don't archive a product, but can only approve or disapprove products created by users."
		})		
	}
	return Product.findById(data.productId).then((foundProduct) => {
		if(!foundProduct.isActive){
			return {
				isSuccess: false,
				message: "Product is already in archive."
			}
		}
		if(foundProduct.userId !== data.userId){
			return {
			isSuccess: false,
			message: "You cannot archive a product you do not own!"
			}				
		}
		return Product.findByIdAndUpdate(data.productId, {isActive: false}).then((archivedProduct, error) => {
			if(!error){
				return {
					isSuccess: true,
					message: 'Product has been archived.'
				}
			}
			return {
				isSuccess: false,
				message: 'Something went wrong!'
			}
			
		})		
	})
}

//UNARCHIVE A PRODUCT
module.exports.unArchive = (data) => {
	if(data.isAdmin){
		return Promise.resolve({
		isSuccess: false,
		message: "An admin don't archive a product, but can only approve or disapprove products created by users."
		})		
	}
	return Product.findById(data.productId).then((foundProduct) => {
		if(foundProduct.isActive){
			return {
				isSuccess: false,
				message: "Product is already enabled."
			}
		}
		if(foundProduct.userId !== data.userId){
			return {
				isSuccess: false,
				message: "You cannot archive a product you do not own!"
			}				
		}
		return Product.findByIdAndUpdate(data.productId, {isActive: true}).then((archivedProduct, error) => {
			if(!error){
				return {
					isSuccess: true,
					message: 'Product has been enabled.'
				}
			}
			return {
				isSuccess: false,
				message: 'Something went wrong!'
			}
			
		})		
	})
}

//SEARCH A PRODUCT BY NAME or Brand - this can also be done by unauthenticated user. Just like lazada and shopee websites
module.exports.searchProductsByName = (searchKeyword) => {
	return Product.find(
		{$and: [
				{$or: [
						{productName: {$regex: searchKeyword, $options: "$i"}},
						{productBrand: {$regex: searchKeyword, $options: "$i"}}
					]
				},
				{isActive: true},
				{isApprovedByAdmin: true}
			]
		},
		{	productName: 1,
			productBrand: 1,
			productDescription: 1,
			price: 1,
			stocksAvailable: 1
		}
	).then((foundProducts) => {
		if(foundProducts.length < 1){
			return {message: 'No product matches the search term.'}
		}
		return foundProducts
	})
}

//FOR ADMIN TO SEARCH ANY PRODUCT
module.exports.searchProductsByNameAdmin = (searchKeyword) => {
	return Product.find(		
		{$or: [
				{productName: {$regex: searchKeyword, $options: "$i"}},
				{productBrand: {$regex: searchKeyword, $options: "$i"}}
			]
		}
	).then((foundProducts) => {
		if(foundProducts.length < 1){
			return {message: 'No product matches the search term.'}
		}
		return foundProducts
	})
}

//For sellers to search from their own products
module.exports.searchFromMyProductsByName = (data) => {
	if(data.isAdmin){
		return {
			isSuccess: false,
			message: "An admin don't sell a product!"
		}
	}
	return Product.find(
		{$and: [
				{$or: [
						{productName: {$regex: data.searchKeyword, $options: "$i"}},
						{productBrand: {$regex: data.searchKeyword, $options: "$i"}}
					]
				},
				{userId: data.userId}
			]
		}
	).then((foundProducts) => {
		if(foundProducts.length < 1){
			return {message: 'None of your products matches the search term.'}
		}
		return foundProducts
	})
}

//UPDATE PRODUCT (user) - Only the user who created the product can update the product
module.exports.updateProduct = async (userData, productId, newData) => {	
	let productUserId = await Product.findById(productId).then((foundProduct) => foundProduct.userId)
	if(userData.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: 'An admin do not update a product, but can approve or disapprove or ban a product.'
		})
	}
	if(userData.userId !== productUserId){
		return Promise.resolve({
			isSuccess: false,
			message: 'You cannot update a product you do not own.'
		})
	}
	return Product.findByIdAndUpdate(
		{_id: productId},
		{
			productName: newData.productName,
			productBrand: newData.productBrand,
			productDescription: newData.productDescription,
			price: newData.price,
			stocksAvailable: newData.stocksAvailable
		},
		{new: true}
	).then((updatedProduct, error) => {
		if(error){
			return {
				isSuccess: false,
				message: 'Something went wrong!'
			}
		}
		return {
			isSuccess: true,
			message: 'Product updated successfully!'
		}
	})
}

//GET SINGLE PRODUCT
module.exports.getSingleProduct = async (productId) => {
	let product = await Product.findById(productId)
	let sellerName = await User.findById(product.userId).then((foundUser) => {
		return {
			firstName: foundUser.firstName,
			lastName: foundUser.lastName
		}
	})
	return {
		sellerId: product.userId,
		productName: product.productName,
		productBrand: product.productBrand,
		productDescription: product.productDescription,
		price: product.price,
		stocksAvailable: product.stocksAvailable,
		sellerFirstName: sellerName.firstName,
		sellerLastName: sellerName.lastName
	}
}