const Cart = require('../models/Cart')
const Product = require('../models/Product')
const Order = require('../models/Order')

//ADD TO CART (user only)
module.exports.addToCart = async (data) => {	
	if(data.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't order!"
		})
	}
	let productData = await Product.findById(data.productId)	
	if(productData.userId == data.userId){
		return Promise.resolve({
			isSuccess: false,
			message: 'You cannot add to cart your own product!'
		})
	}
	let myCart = await Cart.find({userId: data.userId})
	let itemToBeAdded = {
		sellerId: productData.userId,
		productId: productData._id,
		productName: productData.productName,
		productBrand: productData.productBrand,
		price: productData.price,
		quantity: data.quantity,
		subtotal: productData.price * data.quantity
	}	
	if(myCart.length > 0){
		return Cart.findOneAndUpdate(
			{userId: data.userId},
			{
				$push: {products: itemToBeAdded}, 
				totalAmount: myCart[0].totalAmount + itemToBeAdded.subtotal
			},
			{new: true}
		).then((savedCart, error) => {
			if(error){
				return {
					isSuccess: false,
					message: 'Something went wrong!'
				}
			}
			return {
				isSuccess: true,
				message: 'Item added to cart successfully!'
			}
		})
	}
	let newCart = new Cart({
		userId: data.userId,
		products: [
			itemToBeAdded
		],
		totalAmount: itemToBeAdded.subtotal
	})
	return newCart.save().then((savedCart, error) => {
		if(error){
			return error
		}
		return Promise.resolve({
			isSuccess: true,
			message: 'Item added to cart successfully!'
		})
	})
}

//RETRIEVED LOGGED-IN-USER'S CART ITEMS
module.exports.getCartItems = async (userData) => {
	if(userData.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't order!"
		})
	}
	return Cart.find({userId: userData.userId}, {userId: 0}).then((cartItems, error) => {
		if(error){
			return {
				isSuccess: false,
				message: "Something went wrong!"
			}
		}
		return cartItems
	})
}

//CHECK OUT THE ITEMS IN THE CART OF A USER
module.exports.checkOut = async (data) => {
	if(data.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't order!"
		})
	}
	let myCart = await Cart.find({userId: data.userId}).then((foundCart) => foundCart)
	if(myCart.length < 1){
		return Promise.resolve({
			isSuccess: false,
			message: 'No items in your cart to be checked out! Add one first.'
		})
	}
	let amountPaid = 0
	let isPaid = false
	if(data.paymentMethod !== "cash-on-delivery"){
		if(data.amountPaid !== myCart[0].totalAmount){
			return Promise.resolve({
				isSuccess: false,
				message: "Amount paid should be equal to total payment if not cash-on-delivery payment method is selected."
			})
		}
		amountPaid = data.amountPaid
		isPaid = true
	}
	let cartItems = myCart[0].products
	let newOrder = new Order({
		userId: myCart[0].userId,
		products: cartItems,
		totalAmount: myCart[0].totalAmount,
		paymentMethod: data.paymentMethod,
		amountPaid: amountPaid,
		isPaid: isPaid
	})
	return newOrder.save().then((savedOrder, error) => {
		if(error){
			return {
				isSuccess: false,
				message: "Something went wrong!"
			}
		}
		return Cart.findOneAndDelete({userId: data.userId}).then((deleteCartItems, error) => {
			if(error){
				return {
					isSuccess: false,
					message: "Something went wrong!"
				}
			}
			return {
				isSuccess: true,
				message: "Order has been placed."
			}
		})		
	})
}

//CHANGE PRODUCT QUANTITY FROM THE CART
module.exports.updateQuantity = async (data) => {
	if(data.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't order or update an ordered product quantity."
		})		
	}
	let myCart = await Cart.find({userId: data.userId}).then((foundCart) => foundCart)
	if(myCart.length < 1){
		return Promise.resolve({
			isSuccess: false,
			message: "Your cart is empty! Nothing to change quantity."
		})
	}
	let productIndex = 	myCart[0].products.findIndex(orderedProduct => orderedProduct._id == data.productId)
	if(data.newQty < 0){
		if(myCart[0].products[productIndex].quantity > 1){
			myCart[0].products[productIndex].quantity = myCart[0].products[productIndex].quantity + data.newQty
		}
	}else{
		myCart[0].products[productIndex].quantity = myCart[0].products[productIndex].quantity + data.newQty
	}
	myCart[0].products[productIndex].subtotal = myCart[0].products[productIndex].quantity * myCart[0].products[productIndex].price	
	let newTotalAmount = 0
	for(let i = 0; i < myCart[0].products.length; i++){
		newTotalAmount += myCart[0].products[i].subtotal
	}
	return Cart.findOneAndUpdate(
			{userId: data.userId},
			{
				products: myCart[0].products,
				totalAmount: newTotalAmount
			},
			{new: true}
		).then((updatedCart, error) => {
			if(error){
				return error
			}
			return {
				isSuccess: true,
				message: "Quantity updated!"
			}
		})
}

//REMOVE PRODUCT FROM THE CART
module.exports.removeItem = async (data) => {
	if(data.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't order or remove an item from the cart."
		})	
	}
	let myCart = await Cart.find({userId: data.userId}).then((foundCart) => foundCart)
	if(myCart.length < 1){
		return Promise.resolve({
			isSuccess: false, 
			message: "Your cart is empty! Nothing to change quantity."
		})
	}
	let productIndex = 	myCart[0].products.findIndex(orderedProduct => orderedProduct._id == data.productId)
	myCart[0].products.splice(productIndex, 1)
	let newTotalAmount = 0
	for(let i = 0; i < myCart[0].products.length; i++){
		newTotalAmount += myCart[0].products[i].subtotal
	}
	return Cart.findOneAndUpdate(
			{userId: data.userId},
			{
				products: myCart[0].products,
				totalAmount: newTotalAmount
			},
			{new: true}
		).then((updatedCart, error) => {
			if(error){
				return {
					isSuccess: false,
					message: "Something went wrong!"
				}
			}
			return {
				isSuccess: true,
				message: "Item removed successfully!"
			}
		})
} 