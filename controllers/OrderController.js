const Order = require('../models/Order')

//GET LOGGED-IN-USER'S ORDERS 
module.exports.getMyOrders = (userData) => {
	if(userData.isAdmin){
		return Promise.resolve({message: "An admin don't order."})
	}
	return Order.find({userId: userData.userId}, {userId: 0}).sort({_id: -1}).then((foundOrders) => foundOrders)
}

//GET ALL ORDERS FROM ALL USERS (admin only)
module.exports.getAllOrders = (isAdmin) => {
	if(!isAdmin){
		return Promise.resolve(
			{
				typeOfError: 'Accessibility error',
				message: "Only an admin can access this."
			}
		)
	}
	return Order.find({}).then((foundOrders) => foundOrders)
}

//GET ALL ORDERS FROM LOGGED-IN-USER'S PRODUCTS
// module.exports.getMyProductsOrders = async (userData) => {
// 	if(userData.isAdmin){
// 		return Promise.resolve({message: "An admin don't sell any product."})
// 	}
// 	let orders = await Order.find({}).then((foundOrders) => foundOrders)
// 	let myProductsOrders = []
// 	for(let i = 0; i < orders.length; i++){
// 		for(let j = 0; j < orders[i].products.length; j++){
// 			if(orders[i].products[j].sellerId == userData.userId){
// 				myProductsOrders.push(orders[i].products[j])
// 			}
// 		}
// 	}
// 	return Promise.resolve(myProductsOrders)
// }

//GET ALL ORDERS FROM LOGGED-IN-USER'S PRODUCTS
module.exports.getMyProductsOrders = async (userData) => {
	if(userData.isAdmin){
		return Promise.resolve({message: "An admin don't sell any product."})
	}
	let orders = await Order.find({}).sort({_id: -1}).then((foundOrders) => foundOrders)
	let myProductsOrders = []
	for(let i = 0; i < orders.length; i++){
		for(let j = 0; j < orders[i].products.length; j++){
			if(orders[i].products[j].sellerId == userData.userId){
				const orderedProduct = {
					customerId: orders[i].userId,
					sellerId: orders[i].products[j].sellerId,
			        productId: orders[i].products[j].productId,
			        productName: orders[i].products[j].productName,
			        productBrand: orders[i].products[j].productBrand,
			        price: orders[i].products[j].price,
			        quantity: orders[i].products[j].quantity,
			        subtotal: orders[i].products[j].subtotal,
			        status: orders[i].products[j].status,
			        _id: orders[i].products[j]._id
				}	
				myProductsOrders.push(orderedProduct)				
			}
		}
	}	
	return Promise.resolve(myProductsOrders)
}

//GET ALL PENDING ORDERS FROM LOGGED-IN-USER'S PRODUCTS
module.exports.getMyProductsOrdersPending = async (userData) => {
	if(userData.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't sell any product."
		})
	}
	let orders = await Order.find({}).sort({_id: -1}).then((foundOrders) => foundOrders)
	let myProductsOrdersPending = []
	for(let i = 0; i < orders.length; i++){
		for(let j = 0; j < orders[i].products.length; j++){
			if(orders[i].products[j].sellerId == userData.userId && orders[i].products[j].status == "pending"){
				const orderedProduct = {
					customerId: orders[i].userId,
					sellerId: orders[i].products[j].sellerId,
			        productId: orders[i].products[j].productId,
			        productName: orders[i].products[j].productName,
			        productBrand: orders[i].products[j].productBrand,
			        price: orders[i].products[j].price,
			        quantity: orders[i].products[j].quantity,
			        subtotal: orders[i].products[j].subtotal,
			        status: orders[i].products[j].status,
			        _id: orders[i].products[j]._id
				}
				myProductsOrdersPending.push(orderedProduct)
			}
		}
	}
	return Promise.resolve(myProductsOrdersPending)
}

//ACCEPT ORDER
module.exports.acceptOrder = async (data) => {
	if(data.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't sell any product. And also don't accept order."
		})
	}
	let orders = await Order.find({}).then((foundOrders) => foundOrders)
	let order = null
	let orderedProductIndex = null
	let breakCheck = false
	for(let i = 0; i < orders.length; i++){
		for(let j = 0; j < orders[i].products.length; j++){
			if(orders[i].products[j]._id == data.orderProductId){
				order = orders[i]
				orderedProductIndex = j
				breakCheck = true
				break
			}
		}
		if(breakCheck){
			break
		}
	}
	order.products[orderedProductIndex].status = "accepted"	
	return Order.findOneAndUpdate({_id: order._id}, {products: order.products}).then((update, error) =>{
		if(error){
			return {
				isSuccess: false,
				message: "Something went wrong!"
			}
		}
		return {
			isSuccess: true,
			message: "Order successfully accepted!"
		}
	})
}

//UPDATE ORDER STATUS TO SHIPPED
module.exports.shipOrder = async (data) => {
	if(data.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't sell any product. And also don't ship an order."
		})
	}
	let orders = await Order.find({}).then((foundOrders) => foundOrders)
	let order = null
	let orderedProductIndex = null
	let breakCheck = false
	for(let i = 0; i < orders.length; i++){
		for(let j = 0; j < orders[i].products.length; j++){
			if(orders[i].products[j]._id == data.orderProductId){
				order = orders[i]
				orderedProductIndex = j
				breakCheck = true
				break
			}
		}
		if(breakCheck){
			break
		}
	}
	order.products[orderedProductIndex].status = "shipped"	
	return Order.findOneAndUpdate({_id: order._id}, {products: order.products}).then((update, error) =>{
		if(error){
			return {
				isSuccess: false,
				message: "Something went wrong!"
			}
		}
		return {
			isSuccess: true,
			message: "Order status successfully changed to shipped!"
		}
	})
}

//UPDATE ORDER STATUS TO DELIVERED
module.exports.deliverOrder = async (data) => {
	if(data.isAdmin){
		return Promise.resolve({
			isSuccess: false,
			message: "An admin don't sell any product. And also don't deliver an order."
		})
	}
	let orders = await Order.find({}).then((foundOrders) => foundOrders)
	let order = null
	let orderedProductIndex = null
	let breakCheck = false
	for(let i = 0; i < orders.length; i++){
		for(let j = 0; j < orders[i].products.length; j++){
			if(orders[i].products[j]._id == data.orderProductId){
				order = orders[i]
				orderedProductIndex = j
				breakCheck = true
				break
			}
		}
		if(breakCheck){
			break
		}
	}
	order.products[orderedProductIndex].status = "delivered"	
	return Order.findOneAndUpdate({_id: order._id}, {products: order.products}).then((update, error) =>{
		if(error){
			return {
				isSuccess: false,
				message: "Something went wrong!"
			}
		}
		return {
			isSuccess: true,
			message: "Order status successfully changed to delivered!"
		}
	})
}